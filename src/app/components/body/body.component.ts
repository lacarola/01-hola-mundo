import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  frase: any;
  mostrar: boolean;
  personajes: string[];

  constructor() {
    this.mostrar = true;
    this.frase = {
      mensaje: 'Un gran poder conlleva una gran responsabilidad',
      autor: 'Ben Parker'
    };
    this.personajes = ['spiderman', 'Venom', 'Dr. Octopus'];
  }

  ngOnInit() {
  }

}
